Possible performance optimizations
==================================

1. Reduce the number of trips tp the server.
2. Reduce the amount of data passed to/from the server (reduce to just form data not including html code).


What I could have done better
=============================

1. I could put the form html in a single file then use jQuery and javascript for displaying it a section at a time like tabs.
2. I could improve on the UI to show the existence of other section so users would see the nuber of steps involved.
3. I could create a function to handle all insert, a function to handle all select and a function to handle all update. I would just need to pass the data, table name, column name or where clause where they apply.

Pattern used
============
MVC (Model View COntroller): I have chosen this pattern because of the size of the project and because I am very familiar with it.
For larger project MVP, MVI or MVVM would be great.
Since its a small project, it requires very minimal maintainabilty and scalabilty which MVC pattern provides with its seperation of concerns.