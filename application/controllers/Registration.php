<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('registration_model');
		$this->load->helper('form');
		$this->load->library('form_validation');

		//destroy cookie if record does not exist
		if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){
			$personal_data = $this->registration_model->get_personal($_COOKIE['user_id']);
			if(!isset($personal_data)){
				unset($_COOKIE['user_id']);
				unset($_COOKIE['reg_stage']);
    			setcookie('user_id', '', time() - 3600, '/');
    			setcookie('reg_stage', '', time() - 3600, '/');
			}
		}
	}

	// loads intial page
	public function index()
	{
		$data['title'] = "Register Here";
		$this->load->view('shared/header', $data);
		$this->load->view('src/registration_form');
		$this->load->view('shared/footer');
	}

	// initialize form based on cokie value
	public function initialize_form()
	{
		if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){
			if($_COOKIE['reg_stage'] == 'personal'){
				self::personal_form();
			}else if($_COOKIE['reg_stage'] == 'address'){
				self::get_address_form();
			}else if($_COOKIE['reg_stage'] == 'payment'){
				self::get_payment_form();
			}
		}else{
			self::personal_form();
		}
	}

	// loads up the first view contain personal information
	public function personal_form()
	{
		$personal_data = array();
		//checks if there is a cookie to preload previously submitted value
		if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){
			$personal_data = $this->registration_model->get_personal($_COOKIE['user_id']);
		}

		$firstname 	= (isset($personal_data['Firstname'])) ? $personal_data['Firstname'] : '';
		$lastname 	= (isset($personal_data['Lastname'])) ? $personal_data['Lastname'] : '';
		$email 		= (isset($personal_data['EmailAddress'])) ? $personal_data['EmailAddress'] : '';
		$phone 		= (isset($personal_data['Phone'])) ? $personal_data['Phone'] : '';
		
		$res = '<div class = "form-group">
				<label class = "control-label">Firstname</label>
				<input type = "text" class = "form-control" name = "firstname" id = "firstname" value= "'.$firstname.'" required>
			</div>
			<div class = "form-group">
				<label class = "control-label">Lastname</label>
				<input type = "text" class = "form-control" name = "lastname" id = "lastname" value = "'.$lastname.'" required>
			</div>
			<div class = "form-group">
				<label class = "control-label">Email Address</label>
				<input type = "email" class = "form-control" name = "email" id = "email" value = "'.$email.'" required>
			</div>
			<div class = "form-group">
				<label class = "control-label">Phone No</label>
				<input type = "text" class = "form-control" name = "phone" id = "phone" value = "'.$phone.'" required>
			</div>                        
			<div class="row">
				<div class="col-md-6">
					<input type = "submit" class = "btn btn-default btn-lg btn-block" value = "Previous" id = "bio-submit" disabled="disabled">
				</div>
				<div class="col-md-6">
					<input type = "submit" class = "btn btn-success btn-lg btn-block" value = "Next" id = "personal-submit">
				</div>
			</div>';

		$res_array = array("title" => "Personal Information", "content" => $res);
		echo json_encode($res_array);
	}

	// process data from personal information sent from ajax
	public function process_personal()
	{
		if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){
			$res = $this->registration_model->set_personal_data($_COOKIE['user_id']);
		}else{
			$res = $this->registration_model->set_personal_data();
			if($res){			
				setcookie('user_id', $res, strtotime('+1 month', strtotime(date('Y-m-d'))));
				setcookie('reg_stage', 'personal', strtotime('+1 month', strtotime(date('Y-m-d'))));
			}
		}

		if($res){			
			$address_form = self::address_form();
			$res_array = array(
				'status' => 'success',
				'data' => $address_form
			);
			echo json_encode($res_array);
		}else{
			$res_array = array(
				'status' => 'failed'				
			);
			echo json_encode($res_array);
		}
	}

	// displays form 
	public function get_address_form()
	{
		echo json_encode(self::address_form());
	}

	// loads second view for address information 
	public function address_form()
	{
		$address_data = array();
		//checks if there is a cookie to preload previously submitted value
		if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){
			$address_data = $this->registration_model->get_address($_COOKIE['user_id']);
		}
		
		$house_address 	= (isset($address_data['HouseAddress'])) ? $address_data['HouseAddress'] : '';
		$house_no 		= (isset($address_data['HouseNo'])) ? $address_data['HouseNo'] : '';
		$zip_code 		= (isset($address_data['ZipCode'])) ? $address_data['ZipCode'] : '';
		$city 			= (isset($address_data['City'])) ? $address_data['City'] : '';
		
		$res = '<div class = "form-group">
				<label class = "control-label">House Address</label>
				<input type = "text" class = "form-control" name = "house_address" id = "house-address" value = "'.$house_address.'" required>
			</div>
			<div class = "form-group">
				<label class = "control-label">House No</label>
				<input type = "text" class = "form-control" name = "house_no" id = "house-no" value = "'.$house_no.'" required>
			</div>
			<div class = "form-group">
				<label class = "control-label">Zip Code</label>
				<input type = "text" class = "form-control" name = "zip_code" id = "zip-code" value = "'.$zip_code.'" required>
			</div>
			<div class = "form-group">
				<label class = "control-label">City</label>
				<input type = "text" class = "form-control" name = "city" id = "city" value = "'.$city.'" required>
			</div>                        
			<div class="row">
				<div class="col-md-6">
					<input type = "submit" class = "btn btn-default btn-lg btn-block" value = "Previous" id = "personal-form">
				</div>
				<div class="col-md-6">
					<input type = "submit" class = "btn btn-success btn-lg btn-block" value = "Next" id = "address-submit">
				</div>
			</div>';

		$res_array = array("title" => "Address Information", "content" => $res);
		return $res_array;
	}

	// process data from address information sent from ajax
	public function process_address()
	{
		if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){		
			$res = $this->registration_model->set_address_data($_COOKIE['user_id']);
			setcookie('reg_stage', 'address', strtotime('+1 month', strtotime(date('Y-m-d'))));
			if($res){
				$payment_form = self::payment_form();
				$res_array = array(
					'status' => 'success',
					'data' => $payment_form
				);
				echo json_encode($res_array);
			}else{
				$res_array = array(
					'status' => 'failed'				
				);
				echo json_encode($res_array);
			}
		}else{
			$res_array = array(
				'status' => 'failed'				
			);
			echo json_encode($res_array);
		}
	}

	// displays form 
	public function get_payment_form()
	{
		echo json_encode(self::payment_form());
	}

	// loads third view for payment information 
	public function payment_form()
	{
		$payment_data = array();
		//checks if there is a cookie to preload previously submitted value
		if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){
			$payment_data = $this->registration_model->get_payment($_COOKIE['user_id']);
		}
		
		$account_owner 	= (isset($payment_data['AccountOwner'])) ? $payment_data['AccountOwner'] : '';
		$iban 		= (isset($payment_data['IBAN'])) ? $payment_data['IBAN'] : '';
		
		$res = '<div class = "form-group">
				<label class = "control-label">Account Owner</label>
				<input type = "text" class = "form-control" name = "account_owner" id = "account-owner" value = "'.$account_owner.'" required>
			</div>
			<div class = "form-group">
				<label class = "control-label">IBAN</label>
				<input type = "text" class = "form-control" name = "iban" id = "iban" value = "'.$iban.'" required>
			</div>			                       
			<div class="row">
				<div class="col-md-6">
					<input type = "submit" class = "btn btn-default btn-lg btn-block" value = "Previous" id = "address-form">
				</div>
				<div class="col-md-6">
					<input type = "submit" class = "btn btn-success btn-lg btn-block" value = "Submit" id = "payment-submit">
				</div>
			</div>';

		$res_array = array("title" => "Payment Information", "content" => $res);
		return $res_array;
	}

	// process data from payment information sent from ajax
	public function process_payment()
	{
		if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){
			$res = $this->registration_model->set_payment_data($_COOKIE['user_id']);
			if($res){					
				setcookie('reg_stage', 'payment', strtotime('+1 month', strtotime(date('Y-m-d'))));
				$body = array(
					"customerId" => $_COOKIE['user_id'],
					"iban" => $this->input->post('iban'),
					"owner" => $this->input->post('account_owner')
				);
				$api_res = self::curl_function($body);
				if(is_array($api_res) && isset($api_res['paymentDataId'])){					
					$res = $this->registration_model->set_payment_data_id($_COOKIE['user_id'], $api_res['paymentDataId']);
					
					if(isset($_COOKIE['user_id']) && isset($_COOKIE['reg_stage'])){
						unset($_COOKIE['user_id']);
						unset($_COOKIE['reg_stage']);
						setcookie('user_id', '', time() - 3600);
						setcookie('reg_stage', '', time() - 3600);
					}
					
					$res_array = array(
						'status' => 'success',
						"paymentDataId" => $api_res['paymentDataId']						
					);
					echo json_encode($res_array);
				}else{
					$res_array = array(
						'status' => 'failed'				
					);
					echo json_encode($res_array);
				}
			}else{
				$res_array = array(
					'status' => 'failed'				
				);
				echo json_encode($res_array);
			}
		}else{
			$res_array = array(
				'status' => 'failed'				
			);
			echo json_encode($res_array);
		}
	}

	// makes api call
	public function curl_function($body)
	{
		$url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";
		$param = array(
			"Content-Type: application/json"
		);

		if (!empty($url) && !empty($param) && !empty($body)) {
			
			$curl = curl_init($url);
			$curl_post_data = json_encode($body);
			// print_r($curl_post_data);
			// echo "</br></br>";
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $param);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
			
			$curl_response = curl_exec($curl);
			// Closing
			curl_close($curl);
			return json_decode($curl_response, true);
		}else{
			return false;
		}
	}
}
