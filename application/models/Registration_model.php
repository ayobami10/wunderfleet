<?php 
/**
* 
*/
class Registration_model extends CI_Model										
{
	
	function __construct()
	{
		$this->load->database();
		
	}

	public function set_personal_data($user_id = null)
	{
		$data = array(
			'Firstname' => $this->input->post('firstname'),
			'Lastname' => $this->input->post('lastname'), 
			'Phone' => $this->input->post('phone'),
			'EmailAddress' => $this->input->post('email')
		);

		if(!$user_id){
			$ins_res = $this->db->insert('w_registration',$data);
			if($ins_res){
				return $this->db->insert_id();
			}else{
				return false;
			}		
		}else{
			$result = $this->db->update('w_registration', $data, 'ID = '.$user_id);
			if($result){
				return true;
			}else{
				return false;
			}
		}
	}

	public function get_personal($user_id)
	{
		$data = array('ID' => $user_id);

		$query = $this->db->select(array('ID','Firstname', 'Lastname', 'Phone', 'EmailAddress'))
                ->where($data)
                ->get('w_registration');
		$result = $query->row_array();
		
		return $result;
	}

	public function set_address_data($user_id)
	{
		$data = array(
			"HouseAddress" => $this->input->post('house_address'),
			"HouseNo" => $this->input->post('house_no'),
			"ZipCode" => $this->input->post('zip_code'),
			"City" => $this->input->post('city')
		);

		$result = $this->db->update('w_registration', $data, 'ID = '.$user_id);
		if($result){
			return true;
		}else{
			return false;
		}
	}

	
	public function get_address($user_id)
	{
		$data = array('ID' => $user_id);

		$query = $this->db->select(array('ID','HouseAddress', 'HouseNo', 'ZipCode', 'City'))
                ->where($data)
                ->get('w_registration');
		$result = $query->row_array();
		
		return $result;
	}

	public function set_payment_data($user_id)
	{
		$data = array( 
			"AccountOwner" => $this->input->post('account_owner'),
			"IBAN" => $this->input->post('iban')
		);

		$result = $this->db->update('w_registration', $data, 'ID = '.$user_id);
		if($result){
			return true;
		}else{
			return false;
		}
	}

	
	public function get_payment($user_id)
	{
		$data = array('ID' => $user_id);

		$query = $this->db->select(array('ID','AccountOwner', 'IBAN'))
                ->where($data)
                ->get('w_registration');
		$result = $query->row_array();
		
		return $result;
	}

	public function set_payment_data_id($user_id, $PaymentDataId)
	{
		$data = array( 
			"PaymentDataId" => $PaymentDataId,
			"Status" => "1"
		);

		$result = $this->db->update('w_registration', $data, 'ID = '.$user_id);
		if($result){
			return true;
		}else{
			return false;
		}
	}
}
 ?>