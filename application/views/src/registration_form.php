<?php
?>
<div class="row">
    <div class = "col-md-12">
        <div class = "card-block"> 
            <div class = "row">
                <div class = "col-md-12">
                    <div class = "header-block">
                        <h3 class = "text-center">Welcome</h3>
                        <h5 class = "text-center" id = "form-title"></h5>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <form class = "form body-block">
                        <div id = "message-div"></div>
                        <div id = "content-div"></div>                         
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>