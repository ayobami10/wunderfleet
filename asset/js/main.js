$(document).ready(function(){

    $.ajax({
        url: site_url+'/registration',
        type: 'post',
        data: {},
        beforeSend: function(){
            $('#content-div').html('loading...');
        },
        success: function(response){
            res = JSON.parse(response);
            $('#form-title').html(res.title);
            $('#content-div').html(res.content);
        }
    })

    $('#content-div').on('click', '#personal-form', function(e){
        e.preventDefault();
        $.ajax({
            url: site_url+'/registration/personal',
            type: 'post',
            data: { },
            beforeSend: function(){
                $('#content-div').html('loading...');
            },
            success: function(response){
                res = JSON.parse(response);
                $('#form-title').html(res.title);
                $('#content-div').html(res.content);
            }
        })        
    })

    $('#content-div').on('click', '#personal-submit', function(e){
        e.preventDefault();

        var firstname   = $('#firstname').val();
        var lastname    = $('#lastname').val();
        var email       = $('#email').val();
        var phone       = $('#phone').val();
        if(firstname !== '' && lastname !== '' && phone !== '' && email !== ''){
            $.ajax({
                url: site_url+'/registration/process-personal',
                type: 'post',
                data: { firstname: firstname, lastname: lastname, phone: phone, email: email },
                beforeSend: function(){
                    $('#personal-submit').val('processing...');
                    $('#personal-submit').attr('disabled', true);
                },
                success: function(response){
                    res = JSON.parse(response);
                    if(res.status == 'success'){
                        $('#form-title').html(res.data.title);
                        $('#content-div').html(res.data.content);
                    }else{
                        $('#personal-submit').attr('disabled', false);
                        $('#message-div').html(error_message('Something went wrong please try again.'));
                    }
                }
            })
        }else{
            $('#message-div').html(error_message('Please fill all fields correctly'));
        }
    })

    $('#content-div').on('click', '#address-form', function(e){
        e.preventDefault();
        $.ajax({
            url: site_url+'/registration/address',
            type: 'post',
            data: { },
            beforeSend: function(){
                $('#content-div').html('loading...');
            },
            success: function(response){
                res = JSON.parse(response);
                $('#form-title').html(res.title);
                $('#content-div').html(res.content);
            }
        })        
    })

    $('#content-div').on('click', '#address-submit', function(e){
        e.preventDefault();

        var house_address   = $('#house-address').val();
        var house_no        = $('#house-no').val();
        var zip_code        = $('#zip-code').val();
        var city            = $('#city').val();

        if(house_address !== '' && house_no !== '' && zip_code !== '' && city !== ''){
            $.ajax({
                url: site_url+'/registration/process-address',
                type: 'post',
                data: { house_address: house_address, house_no: house_no, zip_code: zip_code, city: city },
                beforeSend: function(){
                    $('#address-submit').val('processing...');
                    $('#address-submit').attr('disabled', true);
                },
                success: function(response){
                    res = JSON.parse(response);
                    if(res.status == 'success'){
                        $('#form-title').html(res.data.title);
                        $('#content-div').html(res.data.content);
                    }else{
                        $('#address-submit').attr('disabled', false);
                        $('#message-div').html(error_message('Something went wrong please try again.'));
                    }
                }
            })
        }else{
            $('#message-div').html(error_message('Please fill all fields correctly'));
        }
    })

    $('#content-div').on('click', '#payment-submit', function(e){
        e.preventDefault();

        var account_owner   = $('#account-owner').val();
        var iban            = $('#iban').val();
        
        if(account_owner !== '' && iban !== ''){
            $.ajax({
                url: site_url+'/registration/process-payment',
                type: 'post',
                data: { account_owner: account_owner, iban: iban },
                beforeSend: function(){
                    $('#payment-submit').val('processing...');
                    $('#payment-submit').attr('disabled', true);
                },
                success: function(response){
                    res = JSON.parse(response);
                    if(res.status == 'success'){
                        $('#form-title').html('');
                        $('#content-div').html(`<h3 class = "text-center">Registration Successful.</h3>                       
                        <h4 class = "text-center" style = "word-break: break-all">Payment ID: ${res.paymentDataId}.</h4>`);
                    }else{
                        $('#payment-submit').attr('disabled', false);
                        $('#message-div').html(error_message('Something went wrong please try again.'));                   
                    }
                }
            })
        }else{
            $('#message-div').html(error_message('Please fill all fields correctly'));
        }
    })
})

function error_message(message) {
    return `<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Error </strong> - ${message}
  </div>`;
}