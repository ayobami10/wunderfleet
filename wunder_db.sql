-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2018 at 11:53 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wunder_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `w_registration`
--

CREATE TABLE `w_registration` (
  `ID` int(11) NOT NULL,
  `Firstname` varchar(255) NOT NULL,
  `Lastname` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `EmailAddress` varchar(255) NOT NULL,
  `HouseAddress` varchar(255) NOT NULL,
  `HouseNo` varchar(10) NOT NULL,
  `ZipCode` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `AccountOwner` varchar(255) NOT NULL,
  `IBAN` varchar(255) NOT NULL,
  `PaymentDataId` varchar(255) NOT NULL,
  `Status` int(10) NOT NULL COMMENT '''0'' for pending or incomplete, ''1'' for successful',
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `w_registration`
--

INSERT INTO `w_registration` (`ID`, `Firstname`, `Lastname`, `Phone`, `EmailAddress`, `HouseAddress`, `HouseNo`, `ZipCode`, `City`, `AccountOwner`, `IBAN`, `PaymentDataId`, `Status`, `DateCreated`) VALUES
(1, 'ayobami', 'omotosho', '1234112', 'ayobamimoses1@gmail.com', '6, eze anthony crescent akesan Lago', '12b', '100001', 'lagos', 'ayobami', '12341asd', '', 0, '2018-10-04 10:01:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `w_registration`
--
ALTER TABLE `w_registration`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `w_registration`
--
ALTER TABLE `w_registration`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
